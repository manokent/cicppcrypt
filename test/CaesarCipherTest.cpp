#include "gtest/gtest.h"
#include "CaesarCipher.h"
#include <string>
 
TEST (CaesarCipherTest, encrypt_OneCharacter_Pass)
{ 
    CaesarCipher cc(1);

    std::string pt("A");
    std::string ct;
    ASSERT_TRUE(cc.encrypt(pt, ct));
    ASSERT_EQ("B", ct);
}
 
TEST (CaesarCipherTest, encrypt_ZCharacter_Pass)
{ 
    CaesarCipher cc(1);

    std::string in("Z");
    std::string out;
    ASSERT_TRUE(cc.encrypt(in, out));
    ASSERT_EQ("A", out);
}

TEST (CaesarCipherTest, encrypt_WithSpace_SpaceNotEncrypted)
{
    CaesarCipher cc(1);

    std::string in("HELLO WORLD");
    std::string out;
    ASSERT_TRUE(cc.encrypt(in, out));
    ASSERT_EQ("IFMMP XPSME", out);
}

TEST (CaesarCipherTest, encrypt_SpecialCharacter_SpecialCharNotEncrypted)
{
    CaesarCipher cc(1);

    std::string in("HELLO@WORLD");
    std::string out;
    ASSERT_TRUE(cc.encrypt(in, out));
    ASSERT_EQ("IFMMP@XPSME", out);
}
 
TEST (CaesarCipherTest, decrypt_OneCharacter_Pass)
{ 
    CaesarCipher cc(1);

    std::string pt("B");
    std::string ct;
    ASSERT_TRUE(cc.decrypt(pt, ct));
    ASSERT_EQ("A", ct);
}
 
TEST (CaesarCipherTest, decrypt_ACharacter_Pass)
{ 
    CaesarCipher cc(1);

    std::string in("A");
    std::string out;
    ASSERT_TRUE(cc.decrypt(in, out));
    ASSERT_EQ("Z", out);
}
 
TEST (CaesarCipherTest, decrypt_ZCharacter_Pass)
{ 
    CaesarCipher cc(1);

    std::string in("Z");
    std::string out;
    ASSERT_TRUE(cc.decrypt(in, out));
    ASSERT_EQ("Y", out);
}
