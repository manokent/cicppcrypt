CC=g++
CFLAGS=-c -Wall -fprofile-arcs -ftest-coverage -I../../3rdParty/googletest-release-1.8.0/googletest/include -I/usr/include -I. -I..
LDFLAGS_TEST=-L. -lgcov -lgtest_main -lgtest -lpthread -lCiCppCrypt
LDFLAGS_EXE=-L. -lgcov -lCiCppCrypt
LDFLAGS_LIB=-static
SOURCES_LIB = CaesarCipher.cpp
SOURCES_EXE = main.cpp
TEST_DIR=test
SOURCES_TEST = $(TEST_DIR)/CaesarCipherTest.cpp
OBJECTS_LIB=$(SOURCES_LIB:.cpp=.o)
OBJECTS_TEST=$(SOURCES_TEST:.cpp=.o)
OBJECTS_EXE=$(SOURCES_EXE:.cpp=.o)
EXECUTABLE=CiCppCrypt
EXECUTABLE_TEST=CiCppCryptTest
TARGET_LIB=libCiCppCrypt.a

all: lib test exe

lib: $(SOURCES_LIB) $(TARGET_LIB)

exe: $(SOURCES_EXE) $(EXECUTABLE)

test: $(SOURCES_TEST) $(EXECUTABLE_TEST)
	
$(TARGET_LIB): $(OBJECTS_LIB)
	ar rcs $(TARGET_LIB) $(OBJECTS_LIB)

$(EXECUTABLE): $(OBJECTS_EXE)
	$(CC) $(OBJECTS_EXE) $(LDFLAGS_EXE) -o $@
	
$(EXECUTABLE_TEST): $(OBJECTS_TEST)
	$(CC) $(OBJECTS_TEST) $(LDFLAGS_TEST) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	rm -f $(TEST_DIR)/*.o
	rm -f $(EXECUTABLE)
	rm -f $(EXECUTABLE_TEST)
	rm -f $(TARGET_LIB)

