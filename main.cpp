#include <iostream>
#include "CaesarCipher.h"

int main(int argc, char** argv)
{
    std::cout << "Cicada CppCrypt" << std::endl;

    CaesarCipher cipher(1);

    std::string original("ABC");

    std::cout << "Original: " << original << std::endl;

    std::string outCipherText;
    if (cipher.encrypt(original, outCipherText))
    {
        std::cout << "Encrypted: " << outCipherText << std::endl;
    }
    else
    {
        std::cout << "Encrypt Failed" << std::endl;
    }

    std::string outPlainText;
    if (cipher.decrypt(outCipherText, outPlainText))
    {
        std::cout << "Decrypted: " << outPlainText << std::endl;
    }
    else
    {
        std::cout << "Decrypt Failed" << std::endl;
    }

	return 1;
}

