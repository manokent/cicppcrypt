#include "CaesarCipher.h"
#include <string>

CaesarCipher::CaesarCipher()
{
}

CaesarCipher::CaesarCipher(int shift)
{
    m_shift = shift;
}

bool CaesarCipher::encrypt(const std::string& plainText, std::string& cipherText)
{
    cipherText = "";

    for (unsigned int x = 0; x < plainText.length(); x++)
    {
        cipherText += caesar(plainText[x], true);
    }

    return true;
}

bool CaesarCipher::decrypt(const std::string& cipherText, std::string& plainText)
{
    plainText = "";

    for (unsigned int x = 0; x < cipherText.length(); x++)
    {
        plainText += caesar(cipherText[x], false);
    }
	
    return true;
}

char CaesarCipher::caesar(char c, bool up)
{
    unsigned int shift = 0;
    if (up)
    {
        shift = m_shift;
    }
    else
    {
        shift = -m_shift;
    }

    if (isalpha(c))
    {
        c = toupper(c); //use upper to keep from having to use two seperate for A..Z a..z
        c = (((c - 65) + shift) % 26) + 65;
    }
    else
    {
        //if c isn't alpha, just send it back.
    }
    return c;
}

