#ifndef CAESAR_CIPHER_H
#define CAESAR_CIPHER_H

#include <string>
#include <iostream>

class CaesarCipher
{
public:

    CaesarCipher(int shift);

    bool encrypt(const std::string& plainText, std::string& cipherText);
    bool decrypt(const std::string& cipherText, std::string& plainText);

private:

    CaesarCipher();

    char caesar(char c, bool up);

    int m_shift;
};

#endif
